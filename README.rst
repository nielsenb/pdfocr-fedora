===============
 pdfocr-fedora
===============

Specfile for packaging the `Poppler fork`_ of `pdfocr`_ for Fedora.

.. _Poppler fork: https://bitbucket.org/nielsenb/pdfocr
.. _pdfocr: https://github.com/gkovacs/pdfocr
