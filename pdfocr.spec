%global shortcommit 4f5750d202d3

Name:		pdfocr
Version:	0.3.0
Release:	3%{?dist}
Summary:	Utility for performing OCR on PDF files
Group:		Applications/Multimedia
License:	MIT
URL:		https://bitbucket.org/nielsenb/pdfocr
Source0:	https://bitbucket.org/nielsenb/pdfocr/get/v%{version}.tar.gz#/%{name}-%{version}.tar.gz
Requires:	ruby >= 1.8.7
Requires:	poppler-utils
Requires:	hocr-tools
Requires:	libxslt

%description
pdfocr adds an OCR text layer to scanned PDF files, allowing them to be searched. It currently depends on Ruby 1.8.7 or above, and uses ocropus, cuneiform, or tesseract for performing OCR.

%prep
%autosetup -n nielsenb-%{name}-%{shortcommit}

%install
install -D %{_builddir}/nielsenb-%{name}-%{shortcommit}/pdfocr.rb %{buildroot}%{_bindir}/pdfocr
install -D %{_builddir}/nielsenb-%{name}-%{shortcommit}/pdfocr.1 %{buildroot}%{_mandir}/pdfocr.1
install -D %{_builddir}/nielsenb-%{name}-%{shortcommit}/fix-hocr.xsl %{buildroot}%{_sysconfdir}/pdfocr/fix-hocr.xsl

%files
%license COPYRIGHT
%{_bindir}/pdfocr
%{_mandir}/pdfocr.1
%{_sysconfdir}/pdfocr/fix-hocr.xsl

%changelog
* Fri Mar 19 2021 Brandon Nielsen <nielsenb@jetfuse.net> 0.3.0-3
- Correct source URL
- Use autosetup

* Wed Oct 10 2018 Brandon Nielsen <nielsenb@jetfuse.net> 0.3.0-2
- Add license
- Remove buildroot cleanup

* Mon Oct 02 2017 Brandon Nielsen <nielsenb@jetfuse.net> 0.3.0-1
- Bump to 0.3.0

* Wed Aug 31 2016 Brandon Nielsen <nielsenb@jetfuse.net> 0.2.0-1
- Bump to 0.2.0

* Wed Mar 18 2015 Brandon Nielsen <nielsenb@jetfuse.net> 0.1.4-1
- Initial specfile
